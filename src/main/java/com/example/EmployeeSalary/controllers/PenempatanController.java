package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Penempatan;
import com.example.EmployeeSalary.modelsDTO.PenempatanDTO;
import com.example.EmployeeSalary.repositories.PenempatanRepository;

@RestController
@RequestMapping("/api")
public class PenempatanController {
	
	@Autowired
	PenempatanRepository penempatanRepository;
	
	//Read All Penempatan
	@GetMapping("penempatan/read")
	public HashMap<String, Object> getAllPenempatan(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Penempatan> listPenempatanEntity = (ArrayList<Penempatan>) penempatanRepository.findAll();
		
		ArrayList<PenempatanDTO> listPenempatanDTO = new ArrayList<PenempatanDTO>();
		
		for(Penempatan penempatan : listPenempatanEntity) {
			PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
			
			listPenempatanDTO.add(penempatanDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Penempatan Data Success");
		result.put("Data", listPenempatanDTO);
		
		return result;
	}
	
	//Create New Penempatan
	@PostMapping("penempatan/create")
	public HashMap<String, Object> creatingPenempatan(@Valid @RequestBody PenempatanDTO penempatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Penempatan penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
		penempatanRepository.save(penempatanEntity);
		
		penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Penempatan Success");
		result.put("Data", penempatanDTO);
		
		return result;
	}

	//Update Penempatan
	@PutMapping("penempatan/update/{id}")
	public HashMap<String, Object> updatingPenempatan(@PathVariable(value = "id")int penempatanID, @Valid @RequestBody PenempatanDTO penempatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Penempatan penempatanEntity = penempatanRepository.findById(penempatanID)
				.orElseThrow(() -> new ResourceNotFoundException("Penempatan", "id", penempatanID));
		penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
		penempatanEntity.setIdPenempatan(penempatanID);
		penempatanRepository.save(penempatanEntity);
		
		penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Penempatan Success");
		result.put("Data", penempatanDTO);
		
		return result;
	}
	
	//Delete Penempatan
	@DeleteMapping("penempatan/delete/{id}")
	public HashMap<String, Object> deletingPenempatan(@PathVariable(value = "id")int penempatanID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Penempatan penempatanEntity = penempatanRepository.findById(penempatanID)
				.orElseThrow(() -> new ResourceNotFoundException("Penempatan", "id", penempatanID));
		
		penempatanRepository.delete(penempatanEntity);
		PenempatanDTO penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Penempatan Success");
		result.put("Data", penempatanDTO);
		
		return result;
	}
}
