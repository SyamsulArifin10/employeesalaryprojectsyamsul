package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Agama;
import com.example.EmployeeSalary.modelsDTO.AgamaDTO;
import com.example.EmployeeSalary.repositories.AgamaRepository;

@RestController
@RequestMapping("/api")
public class AgamaController {
	
	@Autowired
	AgamaRepository agamaRepository;
	
	//Read All Agama
	@GetMapping("agama/read")
	public HashMap<String, Object> getAllAgamaData(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Agama> listAgamaEntity = (ArrayList<Agama>) agamaRepository.findAll();
		
		ArrayList<AgamaDTO> listAgamaDTO = new ArrayList<AgamaDTO>();
		
		for(Agama agama : listAgamaEntity) {
			AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);
			
			listAgamaDTO.add(agamaDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All Agama Data Success");
		result.put("Data", listAgamaDTO);
		
		return result;
	}
	
	//Create New Agama
	@PostMapping("agama/create")
	public HashMap<String, Object> creatingNewAgama(@Valid @RequestBody AgamaDTO agamaDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Agama agamaEntity = modelMapper.map(agamaDTO, Agama.class);
		
		agamaRepository.save(agamaEntity);
		
		agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Agama Success");
		result.put("Data", agamaDTO);
		
		return result;
	}

	//Update Agama
	@PutMapping("agama/update/{id}")
	public HashMap<String, Object> updatingAgama(@PathVariable(value = "id")int agamaID,@Valid @RequestBody AgamaDTO agamaDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Agama agamaEntity = agamaRepository.findById(agamaID)
				.orElseThrow(() -> new ResourceNotFoundException("Agama", "id", agamaID));
		agamaEntity = modelMapper.map(agamaDTO, Agama.class);
		agamaEntity.setIdAgama(agamaID);
		
		agamaRepository.save(agamaEntity);
		agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Agama Success");
		result.put("Data", agamaDTO);
		
		return result;
	}
	
	//Deleting Agama
	@DeleteMapping("agama/delete/{id}")
	public HashMap<String, Object> deletingAgama(@PathVariable(value = "id")int agamaID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Agama agamaEntity = agamaRepository.findById(agamaID)
				.orElseThrow(() -> new ResourceNotFoundException("Agama", "id", agamaID));
		
		agamaRepository.delete(agamaEntity);
		AgamaDTO agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Agama Success");
		result.put("Data", agamaDTO);
		
		return result;
	}
}
