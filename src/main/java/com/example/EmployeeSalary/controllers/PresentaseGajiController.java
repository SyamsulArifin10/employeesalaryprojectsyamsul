package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.PresentaseGaji;
import com.example.EmployeeSalary.modelsDTO.PresentaseGajiDTO;
import com.example.EmployeeSalary.repositories.PresentaseGajiRepository;

@RestController
@RequestMapping("/api")
public class PresentaseGajiController {
	
	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	
	//Read All Presentase Gaji
	@GetMapping("presentaseGaji/read")
	public HashMap<String, Object> getAllPresentaseGaji(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<PresentaseGaji> listPresentaseGajiEntity = (ArrayList<PresentaseGaji>) presentaseGajiRepository.findAll();
		
		ArrayList<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList<PresentaseGajiDTO>();
		
		for(PresentaseGaji presentaseGaji : listPresentaseGajiEntity) {
			PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
			
			listPresentaseGajiDTO.add(presentaseGajiDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Presentase Gaji Data Success ");
		result.put("Data", listPresentaseGajiDTO);
		
		return result;
	}

	//Create Presentase Gaji
	@PostMapping("presentaseGaji/create")
	public HashMap<String, Object> creatingPresentaseGaji(@Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		PresentaseGaji presentaseGaji = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
		
		presentaseGajiRepository.save(presentaseGaji);
		presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Presentase Gaji Success");
		result.put("Data", presentaseGajiDTO);
		
		return result;
	}
	
	//Update Presentase Gaji
	@PutMapping("presentaseGaji/update/{id}")
	public HashMap<String, Object> updatingPresentaseGaji(@PathVariable(value = "id")int presentaseGajiID, @Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(presentaseGajiID)
				.orElseThrow(() -> new ResourceNotFoundException("Presentase Gaji", "id", presentaseGajiID));
		presentaseGaji = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
		presentaseGaji.setIdPresentaseGaji(presentaseGajiID);
		
		presentaseGajiRepository.save(presentaseGaji);
		presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Presentase Gaji Success");
		result.put("Data", presentaseGajiDTO);
		
		return result;
	}
	
	//Delete Presentase Gaji
	@DeleteMapping("presentaseGaji/delete/{id}")
	public HashMap<String, Object> deletingPresentaseGaji(@PathVariable(value = "id")int presentaseGajiID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(presentaseGajiID)
				.orElseThrow(() -> new ResourceNotFoundException("Presentase Gaji", "id", presentaseGajiID));
		presentaseGajiRepository.delete(presentaseGaji);
		
		PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Presentase Gaji Success");
		result.put("Data", presentaseGajiDTO);
		
		return result;
	}
}
