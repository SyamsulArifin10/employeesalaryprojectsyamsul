package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.TunjanganPegawai;
import com.example.EmployeeSalary.modelsDTO.TunjanganPegawaiDTO;
import com.example.EmployeeSalary.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api")
public class TunjanganPegawaiController {
	
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	//Read All Tunjangan Pegawai
	@GetMapping("tunjanganPegawai/read")
	public HashMap<String, Object> getAllTunjanganPegawai(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<TunjanganPegawai> listTunjanganPegawai = (ArrayList<TunjanganPegawai>) tunjanganPegawaiRepository.findAll();
		
		ArrayList<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<TunjanganPegawaiDTO>();
		
		for(TunjanganPegawai tunjanganPegawai : listTunjanganPegawai) {
			TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
			
			listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Tunjangan Pegawai Data Success");
		result.put("Data", listTunjanganPegawaiDTO);
		
		return result;
	}
	
	//Create New Tunjangan Pegawai
	@PostMapping("tunjanganPegawai/create")
	public HashMap<String, Object> creatingTunjanganPegawai(@Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		TunjanganPegawai tunjanganPegawai = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
		tunjanganPegawaiRepository.save(tunjanganPegawai);
		
		tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Tunjangan Pegawai Success");
		result.put("Data", tunjanganPegawaiDTO);
		
		return result;
	}
	
	//Update Tunjangan Pegawai
	@PutMapping("tunjanganPegawai/update/{id}")
	public HashMap<String, Object> updatingTunjanganPegawai(@PathVariable(value = "id")int tunjanganPegawaiID, @Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(tunjanganPegawaiID)
				.orElseThrow(() -> new ResourceNotFoundException("Tunjangan Pegawai", "id", tunjanganPegawaiID));
		tunjanganPegawai = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
		tunjanganPegawai.setIdTunjanganPegawai(tunjanganPegawaiID);
		tunjanganPegawaiRepository.save(tunjanganPegawai);
		
		tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Tunjangan Pegawai Success");
		result.put("Data", tunjanganPegawaiDTO);
		
		return result;
	}

	//Deleting Tunjangan Pegawai
	@DeleteMapping("tunjanganPegawai/delete/{id}")
	public HashMap<String, Object> deletingTunjanganPegawai(@PathVariable(value = "id")int tunjanganPegawaiID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(tunjanganPegawaiID)
				.orElseThrow(() -> new ResourceNotFoundException("Tunjangan Pegawai", "id", tunjanganPegawaiID));
		tunjanganPegawaiRepository.delete(tunjanganPegawai);
		TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Tunjangan Pegawai Success");
		result.put("Data", tunjanganPegawaiDTO);
		
		return result;
	}
}
