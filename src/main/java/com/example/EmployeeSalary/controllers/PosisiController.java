package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Posisi;
import com.example.EmployeeSalary.modelsDTO.PosisiDTO;
import com.example.EmployeeSalary.repositories.PosisiRepository;

@RestController
@RequestMapping("/api")
public class PosisiController {
	
	@Autowired
	PosisiRepository posisiRepository;
	
	//Read All Posisi Data
	@GetMapping("posisi/read")
	public HashMap<String, Object> getAllPosisi(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Posisi> listPosisiEntity = (ArrayList<Posisi>) posisiRepository.findAll();
		
		ArrayList<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
		
		for(Posisi posisi : listPosisiEntity) {
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			
			listPosisiDTO.add(posisiDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Posisi Data Success");
		result.put("Data", listPosisiDTO);
		
		return result;
	}
	
	//Create New Posisi
	@PostMapping("posisi/create")
	public HashMap<String, Object> creatingPosisi(@Valid @RequestBody PosisiDTO posisiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Posisi posisiEntity = modelMapper.map(posisiDTO, Posisi.class);
		posisiRepository.save(posisiEntity);
		
		posisiDTO = modelMapper.map(posisiEntity, PosisiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Posisi Success");
		result.put("Data", posisiDTO);
		
		return result;
	}
	
	//Update Posisi
	@PutMapping("posisi/update/{id}")
	public HashMap<String, Object> updatingPosisi(@PathVariable(value = "id")int posisiID, @Valid @RequestBody PosisiDTO posisiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Posisi posisi = posisiRepository.findById(posisiID)
				.orElseThrow(() -> new ResourceNotFoundException("Posisi", "id", posisiID));
		posisi = modelMapper.map(posisiDTO, Posisi.class);
		posisi.setIdPosisi(posisiID);
		posisiRepository.save(posisi);
		
		posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Posisi Success");
		result.put("Data", posisiDTO);
		
		return result;
	}
	
	//Delete Posisi
	@DeleteMapping("posisi/delete/{id}")
	public HashMap<String, Object> deletingPosisi(@PathVariable(value = "id")int posisiID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Posisi posisi = posisiRepository.findById(posisiID)
				.orElseThrow(() -> new ResourceNotFoundException("Posisi", "id", posisiID));
		
		posisiRepository.delete(posisi);
		
		PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Posisi Success");
		result.put("Data", posisiDTO);
		
		return result;
	}

}
