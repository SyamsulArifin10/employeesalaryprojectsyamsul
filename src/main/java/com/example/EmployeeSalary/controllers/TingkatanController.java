package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Tingkatan;
import com.example.EmployeeSalary.modelsDTO.TingkatanDTO;
import com.example.EmployeeSalary.repositories.TingkatanRepository;

@RestController
@RequestMapping("/api")
public class TingkatanController {
	
	@Autowired
	TingkatanRepository tingkatanRepository;
	
	//Read All Tingkatan
	@GetMapping("tingkatan/read")
	public HashMap<String, Object> getAllTingkatan(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Tingkatan> listTingkatanEntity = (ArrayList<Tingkatan>) tingkatanRepository.findAll();
		
		ArrayList<TingkatanDTO> listTingkatanDTO = new ArrayList<TingkatanDTO>();
		
		for(Tingkatan tingkatan : listTingkatanEntity) {
			TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
			
			listTingkatanDTO.add(tingkatanDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Tingkatan Data Success");
		result.put("Data", listTingkatanDTO);
		
		return result;
	}
	
	//Create New Tingkatan
	@PostMapping("tingkatan/create")
	public HashMap<String, Object> creatingTingkatan(@Valid @RequestBody TingkatanDTO tingkatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Tingkatan tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
		
		tingkatanRepository.save(tingkatanEntity);
		tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Tingkatan Success");
		result.put("Data", tingkatanDTO);
		
		return result;
	}
	
	//Update Tingkatan
	@PutMapping("tingkatan/update/{id}")
	public HashMap<String, Object> updatingTingkatan(@PathVariable(value = "id")int tingkatanID,@Valid @RequestBody TingkatanDTO tingkatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Tingkatan tingkatanEntity = tingkatanRepository.findById(tingkatanID)
				.orElseThrow(() -> new ResourceNotFoundException("Tingkatan", "id", tingkatanID));
		tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
		tingkatanEntity.setIdTingkatan(tingkatanID);
		
		tingkatanRepository.save(tingkatanEntity);
		
		tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Tingkatan Success");
		result.put("Data", tingkatanDTO);
		
		return result;
	}

	//Delete Tingkatan
	@DeleteMapping("tingkatan/delete/{id}")
	public HashMap<String, Object> deletingTingkatan(@PathVariable(value = "id")int tingkatanID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Tingkatan tingkatanEntity = tingkatanRepository.findById(tingkatanID)
				.orElseThrow(() -> new ResourceNotFoundException("Tingkatan", "id", tingkatanID));
		tingkatanRepository.delete(tingkatanEntity);
		
		TingkatanDTO tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Tingkatan Success");
		result.put("Data", tingkatanDTO);
		
		return result;
	}
}
