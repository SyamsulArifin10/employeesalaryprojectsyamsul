package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Karyawan;
import com.example.EmployeeSalary.modelsDTO.KaryawanDTO;
import com.example.EmployeeSalary.repositories.KaryawanRepository;

@RestController
@RequestMapping("/api")
public class KaryawanController {
	
	@Autowired
	KaryawanRepository karyawanRepository;
	
	//Read All Karyawan
	@GetMapping("karyawan/read")
	public HashMap<String, Object> getAllKaryawan(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		ArrayList<Karyawan> listKaryawanEntity = (ArrayList<Karyawan>) karyawanRepository.findAll();
		
		ArrayList<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
		
		for(Karyawan karyawan : listKaryawanEntity) {
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
			
			listKaryawanDTO.add(karyawanDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Karyawan Data Success");
		result.put("Data", listKaryawanDTO);
		
		return result;
	}
	
	//Create New Karyawan
	@PostMapping("karyawan/create")
	public HashMap<String, Object> creatingNewKaryawan(@Valid @RequestBody KaryawanDTO karyawanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Karyawan karyawan = modelMapper.map(karyawanDTO, Karyawan.class);
		karyawanRepository.save(karyawan);
		karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Create New Karyawan Success");
		result.put("Data", karyawanDTO);
		
		return result;
	}

	//Update Karyawan
	@PutMapping("karyawan/update/{id}")
	public HashMap<String, Object> updatingKaryawan(@PathVariable(value = "id")int karyawanID, @Valid @RequestBody KaryawanDTO karyawanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Karyawan karyawanEntity = karyawanRepository.findById(karyawanID)
				.orElseThrow(() -> new ResourceNotFoundException("Karyawan", "id", karyawanID));
		
		karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);
		karyawanEntity.setIdKaryawan(karyawanID);
		
		karyawanRepository.save(karyawanEntity);
		karyawanDTO = modelMapper.map(karyawanEntity, KaryawanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Karyawan Success");
		result.put("Data", karyawanDTO);
		
		return result;
	}
	
	//Delete Karyawan
	@DeleteMapping("karyawan/delete/{id}")
	public HashMap<String, Object> deletingKaryawan(@PathVariable(value = "id")int karyawanID){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		Karyawan karyawan = karyawanRepository.findById(karyawanID)
				.orElseThrow(() -> new ResourceNotFoundException("Karyawan", "id", karyawanID));
		karyawanRepository.delete(karyawan);
		KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Delete Karyawan Success");
		result.put("Data", karyawanDTO);
		
		return result;
	}
}
