package com.example.EmployeeSalary.modelsDTO;

public class UserIdDTO {

	private int idUser;
	private String username;

	public UserIdDTO() {
	}

	public UserIdDTO(int idUser, String username) {
		this.idUser = idUser;
		this.username = username;
	}

	public int getIdUser() {
		return this.idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

//	public boolean equals(Object other) {
//		if ((this == other))
//			return true;
//		if ((other == null))
//			return false;
//		if (!(other instanceof UserId))
//			return false;
//		UserId castOther = (UserId) other;
//
//		return (this.getIdUser() == castOther.getIdUser())
//				&& ((this.getUsername() == castOther.getUsername()) || (this.getUsername() != null
//						&& castOther.getUsername() != null && this.getUsername().equals(castOther.getUsername())));
//	}
//
//	public int hashCode() {
//		int result = 17;
//
//		result = 37 * result + this.getIdUser();
//		result = 37 * result + (getUsername() == null ? 0 : this.getUsername().hashCode());
//		return result;
//	}

}
