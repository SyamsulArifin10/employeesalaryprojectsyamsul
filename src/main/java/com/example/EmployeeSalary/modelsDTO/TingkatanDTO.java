package com.example.EmployeeSalary.modelsDTO;

public class TingkatanDTO {

	private int idTingkatan;
	private String namaTingkatan;

	public TingkatanDTO() {
	}

	public TingkatanDTO(int idTingkatan, String namaTingkatan) {
		this.idTingkatan = idTingkatan;
		this.namaTingkatan = namaTingkatan;
	}

	public int getIdTingkatan() {
		return this.idTingkatan;
	}

	public void setIdTingkatan(int idTingkatan) {
		this.idTingkatan = idTingkatan;
	}

	public String getNamaTingkatan() {
		return this.namaTingkatan;
	}

	public void setNamaTingkatan(String namaTingkatan) {
		this.namaTingkatan = namaTingkatan;
	}
}