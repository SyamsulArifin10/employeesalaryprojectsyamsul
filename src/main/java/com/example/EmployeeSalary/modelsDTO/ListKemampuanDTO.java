package com.example.EmployeeSalary.modelsDTO;

public class ListKemampuanDTO {

	private int idListKemampuan;
	private KaryawanDTO karyawan;
	private KemampuanDTO kemampuan;
	private Integer nilaiKemampuan;

	public ListKemampuanDTO() {
	}

	public ListKemampuanDTO(int idListKemampuan, KaryawanDTO karyawan, KemampuanDTO kemampuan, Integer nilaiKemampuan) {
		this.idListKemampuan = idListKemampuan;
		this.karyawan = karyawan;
		this.kemampuan = kemampuan;
		this.nilaiKemampuan = nilaiKemampuan;
	}

	public int getIdListKemampuan() {
		return this.idListKemampuan;
	}

	public void setIdListKemampuan(int idListKemampuan) {
		this.idListKemampuan = idListKemampuan;
	}

	public KaryawanDTO getKaryawan() {
		return this.karyawan;
	}

	public void setKaryawan(KaryawanDTO karyawan) {
		this.karyawan = karyawan;
	}

	public KemampuanDTO getKemampuan() {
		return this.kemampuan;
	}

	public void setKemampuan(KemampuanDTO kemampuan) {
		this.kemampuan = kemampuan;
	}

	public Integer getNilaiKemampuan() {
		return this.nilaiKemampuan;
	}

	public void setNilaiKemampuan(Integer nilaiKemampuan) {
		this.nilaiKemampuan = nilaiKemampuan;
	}
}
