package com.example.EmployeeSalary.modelsDTO;

import java.math.BigDecimal;

public class TunjanganPegawaiDTO {

	private int idTunjanganPegawai;
	private PosisiDTO posisi;
	private TingkatanDTO tingkatan;
	private BigDecimal besaranTunjanganPegawai;

	public TunjanganPegawaiDTO() {
	}

	public TunjanganPegawaiDTO(int idTunjanganPegawai, PosisiDTO posisi, TingkatanDTO tingkatan,
			BigDecimal besaranTunjanganPegawai) {
		this.idTunjanganPegawai = idTunjanganPegawai;
		this.posisi = posisi;
		this.tingkatan = tingkatan;
		this.besaranTunjanganPegawai = besaranTunjanganPegawai;
	}

	public int getIdTunjanganPegawai() {
		return this.idTunjanganPegawai;
	}

	public void setIdTunjanganPegawai(int idTunjanganPegawai) {
		this.idTunjanganPegawai = idTunjanganPegawai;
	}

	public PosisiDTO getPosisi() {
		return this.posisi;
	}

	public void setPosisi(PosisiDTO posisi) {
		this.posisi = posisi;
	}

	public TingkatanDTO getTingkatan() {
		return this.tingkatan;
	}

	public void setTingkatan(TingkatanDTO tingkatan) {
		this.tingkatan = tingkatan;
	}

	public BigDecimal getBesaranTunjanganPegawai() {
		return this.besaranTunjanganPegawai;
	}

	public void setBesaranTunjanganPegawai(BigDecimal besaranTunjanganPegawai) {
		this.besaranTunjanganPegawai = besaranTunjanganPegawai;
	}

}
