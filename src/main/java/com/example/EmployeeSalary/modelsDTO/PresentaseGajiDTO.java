package com.example.EmployeeSalary.modelsDTO;

import java.math.BigDecimal;

public class PresentaseGajiDTO {

	private int idPresentaseGaji;
	private PosisiDTO posisi;
	private Integer idTingkatan;
	private BigDecimal besaranGaji;
	private Integer masaKerja;

	public PresentaseGajiDTO() {
	}

	public PresentaseGajiDTO(int idPresentaseGaji, PosisiDTO posisi, Integer idTingkatan, BigDecimal besaranGaji,
			Integer masaKerja) {
		this.idPresentaseGaji = idPresentaseGaji;
		this.posisi = posisi;
		this.idTingkatan = idTingkatan;
		this.besaranGaji = besaranGaji;
		this.masaKerja = masaKerja;
	}

	public int getIdPresentaseGaji() {
		return this.idPresentaseGaji;
	}

	public void setIdPresentaseGaji(int idPresentaseGaji) {
		this.idPresentaseGaji = idPresentaseGaji;
	}

	public PosisiDTO getPosisi() {
		return this.posisi;
	}

	public void setPosisi(PosisiDTO posisi) {
		this.posisi = posisi;
	}

	public Integer getIdTingkatan() {
		return this.idTingkatan;
	}

	public void setIdTingkatan(Integer idTingkatan) {
		this.idTingkatan = idTingkatan;
	}

	public BigDecimal getBesaranGaji() {
		return this.besaranGaji;
	}

	public void setBesaranGaji(BigDecimal besaranGaji) {
		this.besaranGaji = besaranGaji;
	}

	public Integer getMasaKerja() {
		return this.masaKerja;
	}

	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}

}