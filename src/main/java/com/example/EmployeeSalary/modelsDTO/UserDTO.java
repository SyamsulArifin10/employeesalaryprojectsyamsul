package com.example.EmployeeSalary.modelsDTO;

import com.example.EmployeeSalary.models.UserId;

public class UserDTO {

	private UserId id;
	private String password;
	private Short status;

	public UserDTO() {
	}

	public UserDTO(UserId id, String password, Short status) {
		this.id = id;
		this.password = password;
		this.status = status;
	}

	public UserId getId() {
		return this.id;
	}

	public void setId(UserId id) {
		this.id = id;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Short getStatus() {
		return this.status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

}

