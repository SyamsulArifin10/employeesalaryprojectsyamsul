package com.example.EmployeeSalary.modelsDTO;

public class AgamaDTO{

	private int idAgama;
	private String namaAgama;

	public AgamaDTO(){
	}

	public AgamaDTO(int idAgama, String namaAgama) {
		this.idAgama = idAgama;
		this.namaAgama = namaAgama;
	}

	public int getIdAgama() {
		return this.idAgama;
	}

	public void setIdAgama(int idAgama) {
		this.idAgama = idAgama;
	}

	public String getNamaAgama() {
		return this.namaAgama;
	}

	public void setNamaAgama(String namaAgama) {
		this.namaAgama = namaAgama;
	}
}

