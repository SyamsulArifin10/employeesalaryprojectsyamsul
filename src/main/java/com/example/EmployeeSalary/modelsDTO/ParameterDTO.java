package com.example.EmployeeSalary.modelsDTO;

import java.math.BigDecimal;
import java.util.Date;

public class ParameterDTO {

	private int idParam;
	private Date tbParameter;
	private BigDecimal TKeluarga;
	private BigDecimal TTransport;
	private BigDecimal PBpjs;
	private BigDecimal lembur;
	private BigDecimal bonusPg;
	private BigDecimal bonusTs;
	private BigDecimal bonusTw;
	private Integer batasanBonusPg;
	private Integer batasanBonusTs;
	private Integer batasanBonusTw;
	private BigDecimal maxBonus;

	public ParameterDTO() {
	}

	public ParameterDTO(int idParam, Date tbParameter, BigDecimal TKeluarga, BigDecimal TTransport, BigDecimal PBpjs,
			BigDecimal lembur, BigDecimal bonusPg, BigDecimal bonusTs, BigDecimal bonusTw, Integer batasanBonusPg,
			Integer batasanBonusTs, Integer batasanBonusTw, BigDecimal maxBonus) {
		this.idParam = idParam;
		this.tbParameter = tbParameter;
		this.TKeluarga = TKeluarga;
		this.TTransport = TTransport;
		this.PBpjs = PBpjs;
		this.lembur = lembur;
		this.bonusPg = bonusPg;
		this.bonusTs = bonusTs;
		this.bonusTw = bonusTw;
		this.batasanBonusPg = batasanBonusPg;
		this.batasanBonusTs = batasanBonusTs;
		this.batasanBonusTw = batasanBonusTw;
		this.maxBonus = maxBonus;
	}

	public int getIdParam() {
		return this.idParam;
	}

	public void setIdParam(int idParam) {
		this.idParam = idParam;
	}

	public Date getTbParameter() {
		return this.tbParameter;
	}

	public void setTbParameter(Date tbParameter) {
		this.tbParameter = tbParameter;
	}

	public BigDecimal getTKeluarga() {
		return this.TKeluarga;
	}

	public void setTKeluarga(BigDecimal TKeluarga) {
		this.TKeluarga = TKeluarga;
	}

	public BigDecimal getTTransport() {
		return this.TTransport;
	}

	public void setTTransport(BigDecimal TTransport) {
		this.TTransport = TTransport;
	}

	public BigDecimal getPBpjs() {
		return this.PBpjs;
	}

	public void setPBpjs(BigDecimal PBpjs) {
		this.PBpjs = PBpjs;
	}

	public BigDecimal getLembur() {
		return this.lembur;
	}

	public void setLembur(BigDecimal lembur) {
		this.lembur = lembur;
	}

	public BigDecimal getBonusPg() {
		return this.bonusPg;
	}

	public void setBonusPg(BigDecimal bonusPg) {
		this.bonusPg = bonusPg;
	}

	public BigDecimal getBonusTs() {
		return this.bonusTs;
	}

	public void setBonusTs(BigDecimal bonusTs) {
		this.bonusTs = bonusTs;
	}

	public BigDecimal getBonusTw() {
		return this.bonusTw;
	}

	public void setBonusTw(BigDecimal bonusTw) {
		this.bonusTw = bonusTw;
	}

	public Integer getBatasanBonusPg() {
		return this.batasanBonusPg;
	}

	public void setBatasanBonusPg(Integer batasanBonusPg) {
		this.batasanBonusPg = batasanBonusPg;
	}

	public Integer getBatasanBonusTs() {
		return this.batasanBonusTs;
	}

	public void setBatasanBonusTs(Integer batasanBonusTs) {
		this.batasanBonusTs = batasanBonusTs;
	}

	public Integer getBatasanBonusTw() {
		return this.batasanBonusTw;
	}

	public void setBatasanBonusTw(Integer batasanBonusTw) {
		this.batasanBonusTw = batasanBonusTw;
	}

	public BigDecimal getMaxBonus() {
		return this.maxBonus;
	}

	public void setMaxBonus(BigDecimal maxBonus) {
		this.maxBonus = maxBonus;
	}
}