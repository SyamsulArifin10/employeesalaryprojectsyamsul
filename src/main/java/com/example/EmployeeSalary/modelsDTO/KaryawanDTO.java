package com.example.EmployeeSalary.modelsDTO;

import java.util.Date;

public class KaryawanDTO{

	private int idKaryawan;
	private AgamaDTO agama;
	private PenempatanDTO penempatan;
	private PosisiDTO posisi;
	private TingkatanDTO tingkatan;
	private String nama;
	private String noKtp;
	private String alamat;
	private Date tanggalLahir;
	private Integer masaKerja;
	private Short statusPernikahan;
	private Date kontrakAwal;
	private Date kontrakAkhir;
	private String jenisKelamin;
	private Integer jumlahAnak;

	public KaryawanDTO() {
	}

	public KaryawanDTO(int idKaryawan, AgamaDTO agama, PenempatanDTO penempatan, PosisiDTO posisi, TingkatanDTO tingkatan, String nama,
			String noKtp, String alamat, Date tanggalLahir, Integer masaKerja, Short statusPernikahan, Date kontrakAwal,
			Date kontrakAkhir, String jenisKelamin, Integer jumlahAnak) {
		this.idKaryawan = idKaryawan;
		this.agama = agama;
		this.penempatan = penempatan;
		this.posisi = posisi;
		this.tingkatan = tingkatan;
		this.nama = nama;
		this.noKtp = noKtp;
		this.alamat = alamat;
		this.tanggalLahir = tanggalLahir;
		this.masaKerja = masaKerja;
		this.statusPernikahan = statusPernikahan;
		this.kontrakAwal = kontrakAwal;
		this.kontrakAkhir = kontrakAkhir;
		this.jenisKelamin = jenisKelamin;
		this.jumlahAnak = jumlahAnak;
	}

	public int getIdKaryawan() {
		return this.idKaryawan;
	}

	public void setIdKaryawan(int idKaryawan) {
		this.idKaryawan = idKaryawan;
	}

	public AgamaDTO getAgama() {
		return this.agama;
	}

	public void setAgama(AgamaDTO agama) {
		this.agama = agama;
	}

	public PenempatanDTO getPenempatan() {
		return this.penempatan;
	}

	public void setPenempatan(PenempatanDTO penempatan) {
		this.penempatan = penempatan;
	}

	public PosisiDTO getPosisi() {
		return this.posisi;
	}

	public void setPosisi(PosisiDTO posisi) {
		this.posisi = posisi;
	}

	public TingkatanDTO getTingkatan() {
		return this.tingkatan;
	}

	public void setTingkatan(TingkatanDTO tingkatan) {
		this.tingkatan = tingkatan;
	}

	public String getNama() {
		return this.nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoKtp() {
		return this.noKtp;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public String getAlamat() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Date getTanggalLahir() {
		return this.tanggalLahir;
	}

	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public Integer getMasaKerja() {
		return this.masaKerja;
	}

	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}

	public Short getStatusPernikahan() {
		return this.statusPernikahan;
	}

	public void setStatusPernikahan(Short statusPernikahan) {
		this.statusPernikahan = statusPernikahan;
	}

	public Date getKontrakAwal() {
		return this.kontrakAwal;
	}

	public void setKontrakAwal(Date kontrakAwal) {
		this.kontrakAwal = kontrakAwal;
	}

	public Date getKontrakAkhir() {
		return this.kontrakAkhir;
	}

	public void setKontrakAkhir(Date kontrakAkhir) {
		this.kontrakAkhir = kontrakAkhir;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public Integer getJumlahAnak() {
		return this.jumlahAnak;
	}

	public void setJumlahAnak(Integer jumlahAnak) {
		this.jumlahAnak = jumlahAnak;
	}
}