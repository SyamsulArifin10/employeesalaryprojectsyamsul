package com.example.EmployeeSalary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeSalary.models.Penempatan;

@Repository
public interface PenempatanRepository extends JpaRepository<Penempatan, Integer>{

}
